from gensim import models
from glovepy import Glove
import os
import re
import csv
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import pickle as p

DATA = 'formal' # formal, social


def w2vParagraphVector(modelw2v, text):
    textVector = 0
    for word in text:
        textVector += modelw2v.wv[word]
    paragraphVector = textVector / len(text)
    return paragraphVector


def gloveParagraphVector(modelGl, text):
    textVector = 0
    for word in text:
        textVector += modelGl.wv[modelGl.word2id[word]]
    paragraphVector = textVector / len(text)
    return paragraphVector


def wtmParagraphVector(modelWtm, text):
    textVector = 0
    for word in text:
        textVector += modelWtm.wv[modelWtm.word2id[word]]
    paragraphVector = textVector / len(text)
    return paragraphVector


def tokenize(dir):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')

    for root, dirs, files in os.walk(dir):
        for file in files:
            print(file)
            tokens = []
            with open(root + '/' + file, 'r') as f:
                text = f.readlines()
                for line in text:
                    if (line != '\n'):
                        line1 = re.sub(r"http\S+", "", line, flags=re.MULTILINE)  # remove links
                        line1 = re.sub(r'[^A-Za-z-\']+', ' ', line1,
                                       flags=re.MULTILINE)  # remove all characters that are not letters, spaces, '-' or '''
                        line1 = line1.lower()
                        toks = tokenizer.tokenize(line1)
                        clean_toks = []
                        for tok in toks:
                            if (tok not in stop_words):
                                clean_toks.append(tok)

            with open(DATA + '/tokens.txt', 'ab') as f:
                writer = csv.writer(f)
                writer.writerows(tokens)
    return text

def addOtherInfo():
    x=1
    #do this before saving into pickle


def main():
        text = tokenize(DATA + '')
        w2vParagraphVectors = []
        glParagraphVectors = []
        wtmParagraphVectors = []
        modelw2v = models.Word2Vec.load(DATA + '/SgHs.txt')
        modelGl = models.Doc2Vec.load(DATA + '/glove.txt')
        modelWtm = Glove.load(DATA + '/wtm')
        for paragraph in text:
            w2vParagraphVectors.append(w2vParagraphVector(modelw2v, paragraph))
            glParagraphVectors.append(gloveParagraphVector(modelGl, paragraph))
            wtmParagraphVectors.append(wtmParagraphVector(modelWtm, paragraph))
        with open('word2vecParagraphVectors.pkl', 'wb') as f:
            p.dump(w2vParagraphVectors, f)
        with open('gloveParagraphVectors.pkl', 'wb') as f:
            p.dump(glParagraphVectors, f)
        with open('wtmParagraphVectors.pkl', 'wb') as f:
            p.dump(wtmParagraphVectors, f)


if __name__ == "__main__":
    main()