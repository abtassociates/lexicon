from utils.reader import readData
from gensim import models
from multiprocessing import cpu_count

DATA = 'formal' #social

def evaluate(model, words):
    for word in words:
        print(word)
        print(model.wv.most_similar(positive=[word, 'vaccine'], negative=['vaccine']))

def dmm(data, files, negative, filename):
    words = ['cancer', 'dtap', 'rt', 'flu', 'influenza', 'hepatitis', 'hib', 'hpv', 'meningitis', 'mmr', 'hesitate',
             'pneumococcal', 'rotavirus', 'shingles', 'varicella', 'chicken', 'pox', 'exemption', 'autism', 'vaccine', 'mercury']
    model = models.Word2Vec(data, workers=cpu_count(), negative=negative, hs=hs, sg=sg)
    print(len(model.wv.vocab))
    model.save(filename)
    if (len(files) > 0):
        for i in range(0, 2):
            newData = readData(files[i])
            model.build_vocab(newData, update=True)
            model.train(newData, total_examples=model.corpus_count, epochs=model.iter)
            print(len(model.wv.vocab))
            model.save('update' + str(i) + filename)
    evaluate(model, words)

def main():
    data = readData(DATA + '/tokens.txt')
    dmm(data, [], 1, DATA + '/DMM.txt')

if __name__ == "__main__":
    main()

