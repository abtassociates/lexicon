import glob
import csv
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk.stem import WordNetLemmatizer
import codecs
from gensim import models
from glovepy import Glove
import pickle as pc

states_abbreviations = {'AL': 'Alabama', 'AK': 'Alaska','AZ': 'Arizona', 'AR': 'Arkansas', 'CA': 'California',
                        'CO': 'Colorado', 'CT': 'Connecticut', 'DE': 'Delaware', 'DC': 'District of Columbia',
                        'FL': 'Florida', 'GA': 'Georgia', 'HI': 'Hawaii', 'ID': 'Idaho', 'IL': 'Illinois',
                        'IN': 'Indiana', 'IA': 'Iowa', 'KS': 'Kansas', 'KY': 'Kentucky', 'LA': 'Louisiana',
                        'ME': 'Maine', 'MD': 'Maryland', 'MA': 'Massachusetts', 'MI': 'Michigan', 'MN': 'Minnesota',
                        'MS': 'Mississippi', 'MO': 'Missouri', 'MT': 'Montana', 'NE': 'Nebraska', 'NV': 'Nevada',
                        'NH': 'New Hampshire', 'NJ': 'New Jersey', 'NM': 'New Mexico', 'NY': 'New York',
                        'NC': 'North Carolina', 'ND': 'North Dakota', 'OH': 'Ohio','OK': 'Oklahoma', 'OR': 'Oregon',
                        'PA': 'Pennsylvania', 'RI': 'Rhode Island', 'SC': 'South Carolina', 'SD': 'South Dakota',
                        'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah', 'VT': 'Vermont', 'VA': 'Virginia',
                        'WA': 'Washington', 'WV': 'West Virginia', 'WI': 'Wisconsin', 'WY': 'Wyoming'}
external_jurisdictions = {'AS': 'American Samoa', 'FM': 'Micronesia', 'GU': 'Guam', 'MH': 'Marshall Islands',
                          'MP': 'N. Mariana Islands', 'PW': 'Palau', 'PR': 'Puerto Rico', 'VI': 'Virgin Islands',
                          'FEDERATED STATES OF MICRONESIA': 'Micronesia', 'GUAM': 'Guam', 'MARSHALL ISLANDS':
                              'Marshall Islands', 'NORTHERN MARIANA ISLANDS': 'N. Mariana Islands', 'PUERTO RICO':
                              'Puerto Rico', 'UNITED STATES VIRGIN ISLANDS': 'Virgin Islands'}
city_jurisdictions = {'chicago': 'Chicago', 'houston': 'Houston', 'new york': 'New York City',
                      'nycity': 'New York City', 'philadelphia': 'Philadelphia', 'san antonio': 'San Antonio'}


def tokenize(dir, start_row):
    modelW2v = models.Word2Vec.load('formal/CbowNs.txt')
    modelGl = Glove.load('formal/glove.txt')
    modelWtm = models.Word2Vec.load('formal/SgHs.txt')  # TODO change with wtm model
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()
    for file in glob.glob(dir + "*.csv"):
        print(file)
        row_num = 0
        with codecs.open(file, 'r', encoding='utf-8', errors='ignore') as f:
            reader = csv.reader(f)
            for row in reader:
                if row_num > start_row:
                    try:
                        w2v_tokens = {}
                        gl_tokens = {}
                        wtm_tokens = {}
                        if 'external' not in file:
                            location = row[18].upper()
                            not_city = True
                            for city in city_jurisdictions.keys():
                                if city in row[19]:
                                    location = city_jurisdictions[city]
                                    not_city = False
                            if location in states_abbreviations:
                                location = states_abbreviations[location]
                            elif not_city:
                                location = 'United States'
                        else:
                            location = row[17].upper()
                            if location in external_jurisdictions:
                                location = external_jurisdictions[location]
                        text = row[26]
                        text = re.sub(r"http\S+", "", text, flags=re.MULTILINE)  # remove links
                        text = re.sub(r'@\S+', '', text, flags=re.MULTILINE)  # remove twitter ids from text
                        text = text.lower()
                        toks = tokenizer.tokenize(text)
                        for tok in toks:
                            if (tok not in stop_words):
                                token = lemma.lemmatize(tok)
                                try:
                                    w2v_tokens[token] = modelW2v[token].tolist()
                                    gl_tokens[token] = modelGl.word_vectors[modelGl.word2id[token]].tolist()
                                    wtm_tokens[token] = modelWtm[token].tolist()
                                except KeyError:
                                    error = 'Word not in dictionary'
                        with open('social/' + location + '_words.pc', 'ab') as f:
                            pc.dump(w2v_tokens, f)
                            pc.dump(gl_tokens, f)
                            pc.dump(wtm_tokens, f)
                    except IndexError:
                        print(row)
                row_num += 1


def main():
    tokenize('social/rowData/', 12)


if __name__ == "__main__":
    main()
