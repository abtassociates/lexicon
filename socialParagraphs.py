import glob
import pickle as pc
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk.stem import WordNetLemmatizer
import codecs
import csv
from gensim import models
from glovepy import Glove
from operator import add

states_abbreviations = {'AL': 'Alabama', 'AK': 'Alaska','AZ': 'Arizona', 'AR': 'Arkansas', 'CA': 'California',
                        'CO': 'Colorado', 'CT': 'Connecticut', 'DE': 'Delaware', 'DC': 'District of Columbia',
                        'FL': 'Florida', 'GA': 'Georgia', 'HI': 'Hawaii', 'ID': 'Idaho', 'IL': 'Illinois',
                        'IN': 'Indiana', 'IA': 'Iowa', 'KS': 'Kansas', 'KY': 'Kentucky', 'LA': 'Louisiana',
                        'ME': 'Maine', 'MD': 'Maryland', 'MA': 'Massachusetts', 'MI': 'Michigan', 'MN': 'Minnesota',
                        'MS': 'Mississippi', 'MO': 'Missouri', 'MT': 'Montana', 'NE': 'Nebraska', 'NV': 'Nevada',
                        'NH': 'New Hampshire', 'NJ': 'New Jersey', 'NM': 'New Mexico', 'NY': 'New York',
                        'NC': 'North Carolina', 'ND': 'North Dakota', 'OH': 'Ohio','OK': 'Oklahoma', 'OR': 'Oregon',
                        'PA': 'Pennsylvania', 'RI': 'Rhode Island', 'SC': 'South Carolina', 'SD': 'South Dakota',
                        'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah', 'VT': 'Vermont', 'VA': 'Virginia',
                        'WA': 'Washington', 'WV': 'West Virginia', 'WI': 'Wisconsin', 'WY': 'Wyoming'}
external_jurisdictions = {'AS': 'American Samoa', 'FM': 'Micronesia', 'GU': 'Guam', 'MH': 'Marshall Islands',
                          'MP': 'N. Mariana Islands', 'PW': 'Palau', 'PR': 'Puerto Rico', 'VI': 'Virgin Islands',
                          'FEDERATED STATES OF MICRONESIA': 'Micronesia', 'GUAM': 'Guam', 'MARSHALL ISLANDS':
                              'Marshall Islands', 'NORTHERN MARIANA ISLANDS': 'N. Mariana Islands', 'PUERTO RICO':
                              'Puerto Rico', 'UNITED STATES VIRGIN ISLANDS': 'Virgin Islands'}
city_jurisdictions = {'chicago': 'Chicago', 'houston': 'Houston', 'new york': 'New York City',
                      'nycity': 'New York City', 'philadelphia': 'Philadelphia', 'san antonio': 'San Antonio'}


def tokenize(dir, start_row):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()
    modelW2v = models.Word2Vec.load('formal/CbowNs.txt')
    modelD2v = models.Doc2Vec.load('formal/PVDM.txt')
    modelGl = Glove.load('formal/glove.txt')
    modelWtm = models.Word2Vec.load('formal/SgHs.txt')  # TODO change with wtm model
    for file in glob.glob(dir + "*.csv"):
        print(file)
        tokens = []
        data = {}
        row_num = 0
        with codecs.open(file, 'r', encoding='utf-8', errors='ignore') as f:
            reader = csv.reader(f)
            for row in reader:
                if row_num > start_row:
                    try:
                        time = row[4] + ' ' + row[5]
                        sentiment = row[21]
                        if row[9] != '':    # if there is author add him, otherwise, add source
                            author = row[8]
                        else:
                            author = row[2]
                        link = row[3]
                        if row[12] != '':
                            reach = row[12]
                        else:
                            reach = row[23]
                        if 'external' not in file:
                            location = row[18].upper()
                            not_city = True
                            for city in city_jurisdictions.keys():
                                if city in row[19]:
                                    location = city_jurisdictions[city]
                                    not_city = False
                            if location in states_abbreviations:
                                location = states_abbreviations[location]
                            elif not_city:
                                location = 'United States'
                        else:
                            location = row[17].upper()
                            if location in external_jurisdictions:
                                location = external_jurisdictions[location]
                        original_text = row[26]
                        text = re.sub(r"http\S+", "", original_text, flags=re.MULTILINE)  # remove links
                        text = re.sub(r'@\S+', '', text, flags=re.MULTILINE)  # remove twitter ids from text
                        text = text.lower()
                        toks = tokenizer.tokenize(text)
                        clean_toks = []
                        for tok in toks:
                            if (tok not in stop_words):
                                token = lemma.lemmatize(tok)
                                clean_toks.append(token)
                        tokens.append(clean_toks)
                        k = len(clean_toks)
                        par_cbow_ns = [0] * 50
                        par_glove = [0] * 100
                        par_wtm = [0] * 50
                        for word in clean_toks:
                            try:
                                par_cbow_ns = list(map(add, par_cbow_ns, modelW2v.wv[word]))
                                par_glove = list(map(add, par_glove, modelGl.word_vectors[modelGl.word2id[word]]))
                                par_wtm = list(map(add, par_wtm, modelWtm.wv[word]))  # TODO change for WTM
                            except KeyError:
                                k -= 1
                        if k > 0:
                            par_doc2vec_pvdm = modelD2v.infer_vector(clean_toks).tolist()
                            par_cbow_ns = [x / k for x in par_cbow_ns]
                            par_glove = [x / k for x in par_glove]
                            par_wtm = [x / k for x in par_wtm]
                            if location in data:
                                data[location].append([location, link, original_text, par_doc2vec_pvdm, par_cbow_ns,
                                                         par_glove, par_wtm, time, sentiment, author, reach])
                            else:
                                data[location] = [[location, link, original_text, par_doc2vec_pvdm, par_cbow_ns,
                                                         par_glove, par_wtm, time, sentiment, author, reach]]
                    except IndexError:
                        print(row)
                row_num += 1
        for location in data:
            with open('social/' + location + 'paragraphs.pc', 'ab') as f:
                pc.dump(data[location], f)


def main():
    tokenize('social/rowData/', 12)


if __name__ == "__main__":
    main()