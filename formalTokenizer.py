import os
import re
import csv
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import codecs


def getTokens(paragraph):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()

    line1 = re.sub(r"http\S+", "", paragraph, flags=re.MULTILINE).lower().replace('_', '') # remove links
    toks = tokenizer.tokenize(line1)
    if len(toks) > 3:
        clean_toks = []
        for tok in toks:
            if (tok not in stop_words):
                token = lemma.lemmatize(tok)
                clean_toks.append(token)
        return clean_toks


def tokenize(dir):
    for root, dirs, files in os.walk(dir):
        for file in files:
            print(root + '/' + file)
            tokens = []
            with codecs.open(root + '/' + file, 'r', encoding='utf-8', errors='ignore') as f:
                text = f.readlines()
                paragraph = ''
                for line in text:
                    if line != '\n' and not ('\n' in line and len(paragraph) > 600): # some paragraphs are divided
                        # by two '\n' and some by one. If paragraph is longer than 600 characters we want to break it
                        # as soon as first '\n' is found since paragraphs are probably separated by one '\n' only
                        if not line.replace('\n', '').isdigit():
                            paragraph = paragraph + line + ' '
                    else:
                        clean_toks = getTokens(paragraph)
                        if clean_toks is not None:
                            tokens.append(clean_toks)
                        paragraph = ''
            clean_toks = getTokens(paragraph)
            if clean_toks is not None:
                tokens.append(clean_toks)
            with open('formal/tokens.txt', 'a') as f:
                writer = csv.writer(f)
                writer.writerows(tokens)


def main():
    tokenize('formal/rowData')


if __name__ == "__main__":
    main()