import csv

def readData(fileName):
    data = []
    with open(fileName, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='\"')
        for row in reader:
            data.append(row)
    return data