import os
import re
import pickle as pc
from gensim import models
from glovepy import Glove
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import codecs


def getTokens(paragraph, modelW2v, modelGl, modelWtm, w2v_tokens, gl_tokens, wtm_tokens):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()
    line1 = re.sub(r"http\S+", "", paragraph, flags=re.MULTILINE).lower().replace('_', '') # remove links
    toks = tokenizer.tokenize(line1)
    if len(toks) > 3:
        for tok in toks:
            if (tok not in stop_words):
                token = lemma.lemmatize(tok)
                try:
                    w2v_tokens[token] = modelW2v[token].tolist()
                    gl_tokens[token] = modelGl.word_vectors[modelGl.word2id[token]].tolist()
                    wtm_tokens[token] = modelWtm[token].tolist()
                except KeyError:
                    error = 'Word not in dictionary'


def tokenize(dir):
    modelW2v = models.Word2Vec.load('formal/CbowNs.txt')
    modelGl = Glove.load('formal/glove.txt')
    modelWtm = models.Word2Vec.load('formal/SgHs.txt')  # TODO change with wtm model
    for root, dirs, files in os.walk(dir):
        w2v_tokens = {}
        gl_tokens = {}
        wtm_tokens = {}
        location = root[root.rfind('/') + 1:]
        for file in files:
            print(root + '/' + file)
            with codecs.open(root + '/' + file, 'r', encoding='utf-8', errors='ignore') as f:
                text = f.readlines()
                paragraph = ''
                for line in text:
                    if line != '\n' and not ('\n' in line and len(paragraph) > 600): # some paragraphs are divided
                        # by two '\n' and some by one. If paragraph is longer than 600 characters we want to break it
                        # as soon as first '\n' is found since paragraphs are probably separated by one '\n' only
                        if not line.replace('\n', '').isdigit():
                            paragraph = paragraph + line + ' '
                    else:
                        getTokens(paragraph, modelW2v, modelGl, modelWtm, w2v_tokens, gl_tokens, wtm_tokens)
                        paragraph = ''
            getTokens(paragraph, modelW2v, modelGl, modelWtm, w2v_tokens, gl_tokens, wtm_tokens)
        if gl_tokens != {}:
            with open('formal/' + location + '_words.pc', 'ab') as f:
                pc.dump(w2v_tokens, f)
                pc.dump(gl_tokens, f)
                pc.dump(wtm_tokens, f)


def main():
    tokenize('formal/rowData')


if __name__ == "__main__":
    main()