from gensim import models
from glovepy import Corpus, Glove

DATA = 'formal' # formal, social


def word2vecEval(filename, words):
    model = models.Word2Vec.load(filename)
    for word in words:
        print(word)
        print(model.wv.most_similar(positive=[word, 'vaccine'], negative=['vaccine']))


def doc2vecEval(filename, words):
    model = models.Doc2Vec.load(filename)
    for word in words:
        print(word)
        print(model.wv.most_similar(positive=[word, 'vaccine'], negative=['vaccine'])) #prints most similar words
        new_vector = model.infer_vector([word])
        print(model.docvecs.most_similar([new_vector])) #prints most similar paragraphs' ids


def gloveEval(filename, words):
    model = Glove.load(filename)
    for word in words:
        print(word)
        print(model.most_similar(word))


def main():
    words = ['cancer', 'dtap', 'rt', 'flu', 'influenza', 'hepatitis', 'hib', 'hpv', 'meningitis', 'mmr', 'hesitate',
             'pneumococcal', 'rotavirus', 'shingle', 'varicella', 'chicken', 'pox', 'exemption', 'autism', 'vaccine',
             'mercury']
    word2vecEval(DATA + '/SgHs.txt', words)
    word2vecEval(DATA + '/SgNs.txt', words)
    word2vecEval(DATA + '/CbowHs.txt', words)
    word2vecEval(DATA + '/CbowNs.txt', words)
    gloveEval(DATA + '/glove.txt', words)
    doc2vecEval(DATA + '/PVDM.txt', words)
    doc2vecEval(DATA + '/PVDBOW.txt', words)


if __name__ == "__main__":
    main()
