from utils.reader import readData
from glovepy import Corpus, Glove
from multiprocessing import cpu_count

DATA = 'formal' # formal; social


def evaluate(model, words):
    for word in words:
        print(word)
        print(model.most_similar(word))


def glove(data, files, filename):
    words =  ['cancer', 'dtap', 'rt', 'flu', 'influenza', 'hepatitis', 'hib', 'hpv', 'meningitis', 'mmr', 'hesitate',
             'pneumococcal', 'rotavirus', 'shingle', 'varicella', 'chicken', 'pox', 'exemption', 'autism', 'vaccine', 'mercury']
    if (len(files) > 0):
        for i in range(0, 2):
            data.append(readData(files[i]))
    corpus = Corpus()
    corpus.fit(data)
    glove = Glove(corpus.matrix, d=100)
    glove.fit(epochs=5, no_threads=cpu_count())
    glove.add_dict(corpus.dictionary)
    glove.save(filename)
    evaluate(glove, words)


def main():
        data = readData(DATA + '/tokens.txt') # if data is split in multiple files first should be tokens.txt
        glove(data, [], DATA + '/glove.txt') # other files should be listed in list (second parameter)


if __name__ == "__main__":
    main()

