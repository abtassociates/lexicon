from utils.reader import readData
from gensim import models
from multiprocessing import cpu_count

DATA = 'social' # formal; social

def evaluate(model, words):
    for word in words:
        print(word)
        print(model.wv.most_similar(positive=[word, 'vaccine'], negative=['vaccine']))

def word2vec(data, files, negative, hs, sg, filename):
    words =  ['cancer', 'dtap', 'rt', 'flu', 'influenza', 'hepatitis', 'hib', 'hpv', 'meningitis', 'mmr', 'hesitate',
             'pneumococcal', 'rotavirus', 'shingle', 'varicella', 'chicken', 'pox', 'exemption', 'autism', 'vaccine', 'mercury']
    model = models.Word2Vec(data, workers=cpu_count(), negative=negative, hs=hs, sg=sg, size=50)
    print(len(model.wv.vocab))
    model.save(DATA + '/' + filename)
    if (len(files) > 0):
        for i in range(0, 2):
            newData = readData(files[i])
            model.build_vocab(newData, update=True)
            model.train(newData, total_examples=model.corpus_count, epochs=model.iter)
            print(len(model.wv.vocab))
            model.save(DATA + '/update' + str(i) + filename)
    evaluate(model, words)

def main():
    print(DATA)
    data = readData(DATA + '/tokens.txt') # if data is split in multiple files first should be tokens.txt
    # word2vec(data, [], 0, 1, 1, 'SgHs.txt') # other files should be listed in list (second parameter)
    # word2vec(data, [], 5, 0, 1, 'SgNs.txt')
    word2vec(data, [], 0, 1, 0, 'CbowHs.txt')
    # word2vec(data, [], 5, 0, 0, 'CbowNs.txt')

if __name__ == "__main__":
    main()

