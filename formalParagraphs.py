import os
import re
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import codecs
import pickle as pc
from gensim import models
from glovepy import Glove
from operator import add


def getTokens(paragraph):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()

    line1 = re.sub(r"http\S+", "", paragraph, flags=re.MULTILINE).lower().replace('_', '') # remove links
    toks = tokenizer.tokenize(line1)
    if len(toks) > 3:
        clean_toks = []
        for tok in toks:
            if (tok not in stop_words):
                token = lemma.lemmatize(tok)
                clean_toks.append(token)
        return clean_toks


def add_paragraphs(modelW2v, modelD2v, modelGl, modelWtm, paragraph, tokens, data, location, file):
    clean_toks = getTokens(paragraph)
    if clean_toks is not None:
        k = len(clean_toks)
        par_cbow_ns = [0] * 50
        par_glove = [0] * 100
        par_wtm = [0] * 50
        tokens.append(clean_toks)
        for word in clean_toks:
            try:
                par_cbow_ns = list(map(add, par_cbow_ns, modelW2v.wv[word]))
                par_glove = list(map(add, par_glove, modelGl.word_vectors[modelGl.word2id[word]]))
                par_wtm = list(map(add, par_wtm, modelWtm.wv[word]))  # TODO change for WTM
            except KeyError:
                k -= 1
        if k > 0:
            par_doc2vec_pvdm = modelD2v.infer_vector(clean_toks).tolist()
            par_cbow_ns = [x / k for x in par_cbow_ns]
            par_glove = [x / k for x in par_glove]
            par_wtm = [x / k for x in par_wtm]
            data.append([location, file, paragraph, par_doc2vec_pvdm, par_cbow_ns, par_glove, par_wtm])


def tokenize(dir):
    modelW2v = models.Word2Vec.load('formal/CbowNs.txt')
    modelD2v = models.Doc2Vec.load('formal/PVDM.txt')
    modelGl = Glove.load('formal/glove.txt')
    modelWtm = models.Word2Vec.load('formal/SgHs.txt')  # TODO change with wtm model
    for root, dirs, files in os.walk(dir):
        for file in files:
            data = []
            location = root[root.rfind('/') + 1:]
            print(root + '/' + file)
            tokens = []
            with codecs.open(root + '/' + file, 'r', encoding='utf-8', errors='ignore') as f:
                text = f.readlines()
                paragraph = ''
                for line in text:
                    if line != '\n' and not ('\n' in line and len(paragraph) > 600): # some paragraphs are divided
                        # by two '\n' and some by one. If paragraph is longer than 600 characters we want to break it
                        # as soon as first '\n' is found since paragraphs are probably separated by one '\n' only
                        if not line.replace('\n', '').isdigit():
                            paragraph = paragraph + line + ' '
                    else:
                        add_paragraphs(modelW2v, modelD2v, modelGl, modelWtm, paragraph.replace('\n', ''), tokens, data, location, file)
                        paragraph = ''
            clean_toks = getTokens(paragraph)
            if clean_toks is not None:
                add_paragraphs(modelW2v, modelD2v, modelGl, modelWtm, paragraph.replace('\n', ''), tokens, data, location, file)
            with open('formal/' + location + 'paragraphs.pc', 'ab') as f:
                pc.dump(data, f)


def main():
    tokenize('formal/rowData')


if __name__ == "__main__":
    main()