from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from .forms import FormalParagraphsForm, SearchForm, SocialSearchForm
from .query2words import query2words
from .query2paragraphs import query2paragraphs
from .aggregateByState import aggregate_by_state
import csv


@login_required
def home(request):
    return render(request, 'home.html')


def save_results_csv(filename, word2vec, wtm):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
    writer = csv.writer(response)
    writer.writerow(['Word2vec'])
    writer.writerow(['Result', 'Similarity score', 'Link', 'Date', 'Semantic sentiment', 'Author', 'Reach'])
    writer.writerows(word2vec)
	'''
    writer.writerow(['Glove'])
    writer.writerow(['Result', 'Similarity score', 'Link', 'Date', 'Semantic sentiment', 'Author', 'Reach'])
    writer.writerows(glove)
	'''
    writer.writerow(['Word-topic mixture'])
    writer.writerow(['Result', 'Similarity score', 'Link', 'Date', 'Semantic sentiment', 'Author', 'Reach'])
    writer.writerows(wtm)
    return response


class FormalFileView(TemplateView):
    template_name = 'formalFile.html'

    def get(self, request):
        try:
            filepath = request.GET['location']
            with open(filepath, 'r') as f:
                content = f.read()
            return HttpResponse(content, content_type='text/plain')
        except FileNotFoundError:
            raise Http404()

class FormalParagraphsView(TemplateView):
    template_name = 'formalParagraphs.html'

    def get(self, request):
        form = FormalParagraphsForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = FormalParagraphsForm(request.POST)
        error = ''
        if form.is_valid():
            try:
                query = form.cleaned_data['query']
                location = form.cleaned_data['location'].name
                k = int(form.cleaned_data['results_number'].number)
                download = form.cleaned_data['download']
                aggregate = form.cleaned_data['aggregate_per_state']
                if aggregate:
                    [word2vec, wtm] = aggregate_by_state(query, '/var/www/cdc/formal/', k)
                else:
                    [word2vec, wtm] = query2paragraphs(query, '/var/www/cdc/formal/', location, k)
                if download:
                    return save_results_csv('results.csv', word2vec, wtm)
                return render(request, self.template_name,
                              {'form': form, 'word2vec': word2vec, 'wtm': wtm, 'aggregate':aggregate})
            except KeyError:
                 error = "Requested word is not found in lexicon!"
        return render(request, self.template_name, {'form': form, 'errorMsg':error})


class FormalWordsView(TemplateView):
    template_name = 'formalWords.html'

    def get(self, request):
        form = SearchForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = SearchForm(request.POST)
        error = ''
        if form.is_valid():
            try:
                download = form.cleaned_data['download']
                query = form.cleaned_data['query']
                location = form.cleaned_data['location'].name
                k = int(form.cleaned_data['results_number'].number)
                [word2vec, wtm] = query2words(query, '/var/www/cdc/formal/', location, k)
                if download:
                    return save_results_csv('results.csv', word2vec, wtm)
                return render(request, self.template_name,
                              {'form': form, 'word2vec': word2vec, 'wtm': wtm})
            except KeyError:
                 error = "Requested word is not found in lexicon!"
            except ValueError:
                error = "Invalid query"
        return render(request, self.template_name, {'form': form, 'errorMsg':error})


class SocialParagraphsView(TemplateView):
    template_name = 'socialParagraphs.html'

    def get(self, request):
        form = SocialSearchForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = SocialSearchForm(request.POST)
        error = ''
        if form.is_valid():
            try:
                download = form.cleaned_data['download']
                query = form.cleaned_data['query']
                location = form.cleaned_data['location'].name
                start_date = form.cleaned_data['start_date']
                end_date = form.cleaned_data['end_date']
                k = int(form.cleaned_data['results_number'].number)
                [word2vec, wtm] = query2paragraphs(query, '/var/www/cdc/social/', location,
                                                          k, start_date, end_date)
                if download:
                    return save_results_csv('results.csv', word2vec, wtm)
                return render(request, self.template_name,
                              {'form': form, 'word2vec': word2vec, 'wtm': wtm})
            except KeyError:
                error = "Requested word is not found in lexicon!"
        return render(request, self.template_name, {'form': form, 'errorMsg':error})


class SocialWordsView(TemplateView):
    template_name = 'socialWords.html'

    def get(self, request):
        form = SocialSearchForm()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = SocialSearchForm(request.POST)
        error = ''
        if form.is_valid():
            try:
                download = form.cleaned_data['download']
                query = form.cleaned_data['query']
                k = int(form.cleaned_data['results_number'].number)
                location = form.cleaned_data['location'].name
                [word2vec, wtm] = query2words(query, '/var/www/cdc/social/', location, k)
                if download:
                    return save_results_csv('results.csv', word2vec, wtm)
                return render(request, self.template_name,
                              {'form': form, 'word2vec': word2vec, 'wtm': wtm})
            except KeyError:
                error = "Requested word is not found in lexicon!"
            except ValueError:
                error = "Invalid query"
        return render(request, self.template_name, {'form': form, 'errorMsg':error})