from django.contrib import admin
from .models import Location, Number

admin.site.register(Location)
admin.site.register(Number)