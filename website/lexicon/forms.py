from django import forms
from .models import Location, Number
import datetime


class SearchForm(forms.Form):
    query = forms.CharField(widget=forms.TextInput(attrs={'size': 50}))
    results_number = forms.ModelChoiceField(queryset=Number.objects.all(), initial='10')
    location = forms.ModelChoiceField(queryset=Location.objects.all(), initial='United States')
    download = forms.BooleanField(required=False)


class SocialSearchForm(SearchForm):
    date = datetime.datetime.now().strftime('%m/%d/%Y')
    start_date = forms.DateField(widget=forms.TextInput(attrs={'class':'datepicker', 'value':date}))
    end_date = forms.DateField(widget=forms.TextInput(attrs={'class':'datepicker', 'value':date}))


class FormalParagraphsForm(SearchForm):
    aggregate_per_state = forms.BooleanField(required=False)