from django.db import models


class Location(models.Model):
    name = models.CharField(max_length = 127)

    def __str__(self):
        return self.name

class Number(models.Model):
    number = models.CharField(max_length=3)

    def __str__(self):
        return self.number