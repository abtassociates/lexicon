from gensim import models
from glovepy import Glove
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from operator import add
from scipy import spatial
import codecs
import pickle as pc
import datetime
import numpy as np


def change_element(cos_distances_arr, par_vec, data, ind):
    max_val = max(cos_distances_arr)
    sp_dist = spatial.distance.cosine(par_vec, data[4])
    if max_val > sp_dist:
        max_key = max(cos_distances_arr, key=cos_distances_arr.get)
        del cos_distances_arr[max_key]
        cos_distances_arr[ind] = sp_dist


def add_element(k, cos_distances, data, score, min_max, min_max_ind, social):
    score = round(score, 4)
    if len(cos_distances) == 0 or str(score) not in np.array(cos_distances)[:, 1]:
        if len(cos_distances) < k:
            if social:
                cos_distances.append([data[2], score, data[1], data[7], data[8], data[9], data[10]])
            else:
                cos_distances.append([data[2], score, '/var/www/cdc/formal/rowData/' + data[0] + '/' + data[1]])
            if score < min_max:
                min_max = score
                min_max_ind = len(cos_distances) - 1
        elif min_max < score:
            if social:
                cos_distances[min_max_ind] = [data[2], score, data[1], data[7], data[8], data[9], data[10]]
            else:
                cos_distances[min_max_ind] = [data[2], score, '/var/www/cdc/formal/rowData/' + data[0] + '/' + data[1]]
            min_max = score # min is not the score, but min of elements in list
            for i in range(0, len(cos_distances)):
                if cos_distances[i][1] < min_max:
                    min_max = cos_distances[i][1]
                    min_max_ind = i
    return min_max, min_max_ind


def query2paragraphs(query, folder, location, k = 10, start_date=None, end_date=None):
    social = 'social' in folder
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()
    query = query.replace('  ', ' ')
    modelW2v = models.Word2Vec.load(folder + 'CbowNs.txt')
#    modelGl = Glove.load(folder + 'glove.txt')
    modelWtm = models.Word2Vec.load(folder + 'SgHs.txt')  # TODO change with wtm model
    query = query.lower()
    toks = tokenizer.tokenize(query)
    clean_toks = []
    for tok in toks:
        if (tok not in stop_words):
            token = lemma.lemmatize(tok)
            clean_toks.append(token)
    par_len = len(clean_toks)
    par_cbow_ns = [0] * 50
    par_glove = [0] * 100
    par_wtm = [0] * 50
    for word in clean_toks:
        try:
            par_cbow_ns = list(map(add, par_cbow_ns, modelW2v.wv[word]))
#            par_glove = list(map(add, par_glove, modelGl.word_vectors[modelGl.word2id[word]]))
            par_wtm = list(map(add, par_wtm, modelWtm.wv[word]))  # TODO change for WTM
        except KeyError:
            print(word)
    if par_len > 0:
        par_cbow_ns = [x / par_len for x in par_cbow_ns]
#        par_glove = [x / par_len for x in par_glove]
        par_wtm = [x / par_len for x in par_wtm]
    with codecs.open(folder + location + 'paragraphs.pc', 'rb') as f:
        cos_distances_w2v = []
        cos_distances_gl = []
        cos_distances_wtm = []
        max_w2v = 10
        max_ind_w2v = 0
        max_gl = 10
        max_ind_gl = 0
        max_wtm = 10
        max_ind_wtm = 0
        while True:
            try:
                data = pc.load(f)
                for i in range(0, len(data)):
                    start = ''
                    end = ''
                    if social:
                        start = start_date.strftime('%Y-%m-%d %H:%M:%S')
                        end = (end_date + datetime.timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
                    if not social or (start <= data[i][7] <= end):
                        [max_w2v, max_ind_w2v] = add_element(k, cos_distances_w2v, data[i],
                                        spatial.distance.cosine(par_cbow_ns, data[i][4]), max_w2v, max_ind_w2v, social)
'''
                        [max_gl, max_ind_gl] = add_element(k, cos_distances_gl, data[i],
                                        spatial.distance.cosine(par_glove, data[i][5]), max_gl, max_ind_gl, social)
'''
                        [max_wtm, max_ind_wtm] = add_element(k, cos_distances_wtm, data[i],
                                        spatial.distance.cosine(par_wtm, data[i][6]), max_wtm, max_ind_wtm, social)
            except EOFError:
                break

    cos_distances_w2v = sorted(cos_distances_w2v, key=lambda l: l[1], reverse=True)
#    cos_distances_gl = sorted(cos_distances_gl, key=lambda l: l[1], reverse=True)
    cos_distances_wtm = sorted(cos_distances_wtm, key=lambda l: l[1], reverse=True)

    if len(cos_distances_w2v) == 0:
        cos_distances_w2v.append(['No results for this selection', 'Change parameters!'])
    else:
        max_cos_dist = cos_distances_w2v[0][1]
        for i in range(0, len(cos_distances_w2v)):
            cos_distances_w2v[i][1] = round(cos_distances_w2v[i][1] / max_cos_dist, 3)
'''
    if len(cos_distances_gl) == 0:
        cos_distances_gl.append(['No results for this selection', 'Change parameters!'])
    else:
        max_cos_dist = cos_distances_gl[0][1]
        for i in range(0, len(cos_distances_gl)):
            cos_distances_gl[i][1] = round(cos_distances_gl[i][1] / max_cos_dist, 3)
'''
    if len(cos_distances_wtm) == 0:
        cos_distances_wtm.append(['No results for this selection', 'Change parameters!'])
    else:
        max_cos_dist = cos_distances_wtm[0][1]
        for i in range(0, len(cos_distances_wtm)):
            cos_distances_wtm[i][1] = round(cos_distances_wtm[i][1] / max_cos_dist, 3)
    return cos_distances_w2v, cos_distances_wtm