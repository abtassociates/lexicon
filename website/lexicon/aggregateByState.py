import os
from .query2paragraphs import query2paragraphs
import operator


def aggregate_by_state(query, folder, k):
    data = {}
    scores_w2v = {}
    scores_gl = {}
    scores_wtm = {}
    max_score_w2v = 0
    max_score_gl = 0
    max_score_wtm = 0
    for root, dirs, files in os.walk(folder + 'rowData/'):
        for dir in dirs:
            if dir != 'United States':
                data[dir] = query2paragraphs(query, '/var/www/cdc/formal/', dir, k)
    for location in data:
        try:
            if data[location][0][0][0] != 'No results for this selection':
                scores_w2v[location] = round(sum(float(row[1]) for row in data[location][0]) / len(data[location][0]), 3)
                if scores_w2v[location] > max_score_w2v:
                    max_score_w2v = scores_w2v[location]
'''
                scores_gl[location] = round(sum(float(row[1]) for row in data[location][1]) / len(data[location][1]), 3)
                if scores_gl[location] > max_score_gl:
                    max_score_gl = scores_gl[location]
'''
                scores_wtm[location] = round(sum(float(row[1]) for row in data[location][2]) / len(data[location][2]), 3)
                if scores_wtm[location] > max_score_wtm:
                    max_score_wtm = scores_wtm[location]
        except ZeroDivisionError:
            print(location)

    scores_w2v = {k: round(v / max_score_w2v, 3) for k, v in scores_w2v.items()}
#    scores_gl = {k: round(v / max_score_gl, 3) for k, v in scores_gl.items()}
    scores_wtm = {k: round(v / max_score_wtm, 3) for k, v in scores_wtm.items()}

    scores_w2v = sorted(scores_w2v.items(), key=operator.itemgetter(1), reverse=True)
#    scores_gl = sorted(scores_gl.items(), key=operator.itemgetter(1), reverse=True)
    scores_wtm = sorted(scores_wtm.items(), key=operator.itemgetter(1), reverse=True)
    return scores_w2v, sscores_wtm