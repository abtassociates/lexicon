from gensim import models
from glovepy import Glove
from nltk.stem import WordNetLemmatizer


def andOp(data, dataC):
    i = 0
    while i < len(data):
        wordFound = False
        for wordC in dataC:
            if data[i][0] == wordC[0]:
                wordFound=True
                data[i] = (data[i][0], min(data[i][1], wordC[1]))
        if not wordFound:
            data.remove(data[i])
        else:
            i += 1
    return sorted(data, key=lambda x: x[1], reverse=True)


def orOp(data, dataC):
    for wordC in dataC:
        wordFound = False
        for i in range(0, len(data)):
            if data[i][0] == wordC[0]:
                wordFound = True
                data[i] = (data[i][0], max(data[i][1], wordC[1]))
        if not wordFound:
            data.append(wordC)
    return sorted(data, key=lambda x: x[1], reverse=True)


def notOp(data, dataC, searchedWord):
    for word in data:
        wordFound = False
        for wordC in dataC:
            if word[0] == wordC[0] or word[0] == searchedWord:
                wordFound=True
        if wordFound:
            data.remove(word)
    return sorted(data, key=lambda x: x[1], reverse=True)


def roundSimilarity(data):
    for i in range(0, len(data)):
        data[i] = (data[i][0], round(data[i][1], 2))
    return data


#def andOrNotQuery(lemma, query, modelW2v, modelGl, modelWtm, k):
def andOrNotQuery(lemma, query, modelW2v, modelWtm, k):
    operators = ['AND', 'OR', 'NOT']
    curOp = ''
    word2vec = []
    glove = []
    wtm = []
    words = query.split(' ')
    opRequested = False
    for word in words:
        word = lemma.lemmatize(word)
        if not opRequested and word not in operators:
            word2vecC = roundSimilarity(modelW2v.wv.most_similar(
                positive=[word, 'vaccine'], negative=['vaccine'], topn=10*k))
#            gloveC = roundSimilarity(list(modelGl.most_similar(word, topn=10*k).items()))
            wtmC = roundSimilarity(modelWtm.wv.most_similar(positive=[word, 'vaccine'], negative=['vaccine'], topn=10*k))
            if curOp == '':
                word2vec = word2vecC
#               glove = gloveC
                wtm = wtmC
            elif curOp == 'AND':
                word2vec = andOp(word2vec, word2vecC)
#                glove = andOp(glove, gloveC)
                wtm = andOp(wtm, wtmC)
            elif curOp == 'OR':
                word2vec = orOp(word2vec, word2vecC)
#               glove = orOp(glove, gloveC)
                wtm = orOp(wtm, wtmC)
            else:
                word2vec = notOp(word2vec, word2vecC, word)
#               glove = notOp(glove, gloveC, word)
                wtm = notOp(wtm, wtmC, word)
            opRequested = True
        elif opRequested and word in operators:
            curOp = word
            opRequested = False
        else:
            raise ValueError
    if len(word2vec) == 0:
        word2vec.append(('No results', 'query is too selective!'))
    else:
        max_score = word2vec[0][1]
        for i in range(0, len(word2vec)):
            word2vec[i] = (word2vec[i][0], round(word2vec[i][1] / max_score, 3))
'''
    if len(glove) == 0:
        glove.append(('No results', 'query is too selective!'))
    else:
        max_score = glove[0][1]
        for i in range(0, len(glove)):
            glove[i] = (glove[i][0], round(glove[i][1] / max_score, 3))
'''
    if len(wtm) == 0:
        wtm.append(('No results', 'query is too selective!'))
    else:
        max_score = wtm[0][1]
        for i in range(0, len(wtm)):
            wtm[i] = (wtm[i][0], round(wtm[i][1] / max_score, 3))
	return word2vec[0:min(k, len(word2vec))], wtm[0:min(k, len(wtm))]
#   return word2vec[0:min(k, len(word2vec))], glove[0:min(k, len(glove))], wtm[0:min(k, len(wtm))]


def query2words(query, folder, location, k=10):
    lemma = WordNetLemmatizer()
    query = query.replace('  ', ' ')
    #TODO read file with words available per state and dates in case of social data
    modelW2v = models.Word2Vec.load(folder + 'CbowNs.txt')
#    modelGl = Glove.load(folder + 'glove.txt')
    modelWtm = models.Word2Vec.load(folder + 'SgHs.txt') # TODO change with wtm model
    if len(query.split(' AND ')) > 1 or len(query.split(' OR ')) > 1 or len(query.split(' NOT ')) > 1:
		return andOrNotQuery(lemma, query, modelW2v, modelWtm, k)
#        return andOrNotQuery(lemma, query, modelW2v, modelGl, modelWtm, k)
    else:
        lemma.lemmatize(query)
        word2vec = roundSimilarity(modelW2v.wv.most_similar(positive=[query, 'vaccine'], negative=['vaccine'], topn=k))
#        glove = roundSimilarity(list(modelGl.most_similar(query, topn=k).items()))
        wtm = roundSimilarity(modelWtm.wv.most_similar(positive=[query, 'vaccine'], negative=['vaccine'], topn=k))
        if len(word2vec) == 0:
            word2vec.append(('No results', 'query is too selective!'))
        else:
            max_score = word2vec[0][1]
            for i in range(0, len(word2vec)):
                word2vec[i] = (word2vec[i][0], round(word2vec[i][1] / max_score, 3))
'''
        if len(glove) == 0:
            glove.append(('No results', 'query is too selective!'))
        else:
            max_score = glove[0][1]
            for i in range(0, len(glove)):
                glove[i] = (glove[i][0], round(glove[i][1] / max_score, 3))
'''
        if len(wtm) == 0:
            wtm.append(('No results', 'query is too selective!'))
        else:
            max_score = wtm[0][1]
            for i in range(0, len(wtm)):
                wtm[i] = (wtm[i][0], round(wtm[i][1] / max_score, 3))

#        return word2vec, glove, wtm
		return word2vec, wtm