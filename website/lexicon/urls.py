from django.conf.urls import url
from . import views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout

urlpatterns = [

    url(r'^$', login, {'template_name': 'login.html'}),
    url(r'^logout/$', logout, {'template_name' : 'logout.html'}),
    url(r'^home/$', views.home, name='home.html'),
    url(r'^formalParagraphs/$', login_required(views.FormalParagraphsView.as_view()), name='formalParagraphs'),
    url(r'^formalWords/$', login_required(views.FormalWordsView.as_view()), name='formalWords'),
    url(r'^socialParagraphs/$', login_required(views.SocialParagraphsView.as_view()), name='socialParagraphs'),
    url(r'^socialWords/$', login_required(views.SocialWordsView.as_view()), name='socialWords'),
    url(r'^formalFile/$', login_required(views.FormalFileView.as_view()), name='formalFile')]
