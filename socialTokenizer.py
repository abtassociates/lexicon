import glob
import csv
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk.stem import WordNetLemmatizer
import codecs

def tokenize(dir, start_row):
    tokenizer = RegexpTokenizer(r'\w+')
    stop_words = stopwords.words('english')
    lemma = WordNetLemmatizer()

    for file in glob.glob(dir + "*.csv"):
        print(file)
        tokens = []
        row_num = 0
        with codecs.open(file, 'r', encoding='utf-8', errors='ignore') as f:
            reader = csv.reader(f)
            for row in reader:
                if row_num > start_row:
                    try:
                        text = row[26]
                        text = re.sub(r"http\S+", "", text, flags=re.MULTILINE)  # remove links
                        text = re.sub(r'@\S+', '', text, flags=re.MULTILINE)  # remove twitter ids from text
                        text = text.lower()
                        toks = tokenizer.tokenize(text)
                        clean_toks = []
                        for tok in toks:
                            if (tok not in stop_words):
                                token = lemma.lemmatize(tok)
                                clean_toks.append(token)
                        tokens.append(clean_toks)
                    except IndexError:
                        print(row)
                row_num += 1
        with open('social/tokens.txt', 'a') as f:
            writer = csv.writer(f)
            writer.writerows(tokens)


def main():
    tokenize('social/rowData/', 12)


if __name__ == "__main__":
    main()
