from utils.reader import readData
from gensim import models
from multiprocessing import cpu_count

DATA = 'social' # formal; social

def evaluate(model, words):
    for word in words:
        print(word)
        new_vector = model.infer_vector([word])
        print(model.docvecs.most_similar([new_vector]))

def createSentences(data):
    sentences = []
    i = 0
    for row in data:
        sentences.append(models.doc2vec.TaggedDocument(words=row, tags=[i]))
        i += 1
    return sentences

def doc2vec(data, files, dm, filename):
    words =  ['cancer', 'dtap', 'rt', 'flu', 'influenza', 'hepatitis', 'hib', 'hpv', 'meningitis', 'mmr', 'hesitate',
             'pneumococcal', 'rotavirus', 'shingle', 'varicella', 'chicken', 'pox', 'exemption', 'autism', 'vaccine', 'mercury']
    data = createSentences(data)
    model = models.Doc2Vec(data, workers=cpu_count(), dm=dm, vector_size=50)
    model.save(DATA + '/' + filename)
    if (len(files) > 0):
        for i in range(0, 2):
            newData = createSentences(readData(files[i]))
            model.build_vocab(newData, update=True)
            model.train(newData, total_examples=model.corpus_count, epochs=model.iter)
            model.save(DATA + '/update' + str(i) + filename)
    evaluate(model, words)

def main():
    print(DATA)
    data = readData(DATA + '/tokens.txt') # if data is split in multiple files first should be tokens.txt
    doc2vec(data, [], 1, 'PVDM.txt') # other files should be listed in list (second parameter)
    doc2vec(data, [], 0, 'PVDBOW.txt')

if __name__ == "__main__":
    main()

